import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {RectangleInputModel} from './RectangleInputModel';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import  {
    ShapeConstructionPropertiesProvider
}
    from '../../service/geometry/shape/construction/ShapeConstructionPropertiesProvider';
import {PropertiesWrapper} from '../../model/object/PropertiesWrapper';
import {ShapeFactory} from '../../model/geometry/shape/construction/ShapeFactory';
import {Shape} from '../../model/geometry/shape/Shape';
import {
    CalculateShapePropertyByCalculationType
}
    from '../../service/geometry/shape/CalculateShapePropertyByCalculationType';
import {isPositive} from '../../model/validator/NumberInputValidator';
import {Subscription} from 'rxjs/Subscription';

@Component({
    templateUrl: './calculation-component.html',
    styleUrls: ['./calculation-component.css'],
    selector: 'calculation',
    providers: [
        ShapeConstructionPropertiesProvider,
        ShapeFactory,
        CalculateShapePropertyByCalculationType
    ]
})

export class CalculationComponent implements OnInit, OnDestroy {
    public shapeType: string;
    public calculationType: string;
    public shapeProperties: PropertiesWrapper;
    public shapeInputForm: FormGroup;
    public result: number;
    private shapeInputSubscription: Subscription;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private formsBuilder: FormBuilder,
                private shapeConstructionPropertiesProvider: ShapeConstructionPropertiesProvider,
                private shapeFactory: ShapeFactory,
                private calculateShapePropertyByCalculationType: CalculateShapePropertyByCalculationType) {
    }

    public ngOnInit(): void {
        this.activatedRoute.params.subscribe((params) => {
                this.shapeType = params['shape'].toString().toLowerCase();
                this.calculationType = params['calculationType'].toString().toLowerCase();
                this.shapeProperties = this.shapeConstructionPropertiesProvider.provide(this.shapeType);
                this.initForm();
                this.subscribeForm();
            }
        );
    }

    public goBack(): void {
        this.router.navigateByUrl('');
    }

    public ngOnDestroy(): void {
        this.shapeInputSubscription.unsubscribe();
    }

    private initForm(): void {
        this.shapeInputForm = new FormGroup({});
        for (let prop of this.shapeProperties.getProperties()) {
            this.shapeInputForm.addControl(
                prop, this.formsBuilder.control('',
                    Validators.compose([Validators.required, isPositive]))
            );
        }
    }

    private subscribeForm(): void {
        this.shapeInputSubscription = this.shapeInputForm.valueChanges.subscribe((values) => {
            if (this.shapeInputForm.valid) {
                const shape: Shape = this.shapeFactory.create(this.shapeType, values);
                this.result = this.calculateShapePropertyByCalculationType.calculate(shape, this.calculationType);
            }
        });
    }
}
