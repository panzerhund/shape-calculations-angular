import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {ShapeTypes} from '../../model/geometry/shape/ShapeTypes';
import {CalculationTypes} from '../../model/geometry/shape/CalculationTypes';
@Component({
    templateUrl: './calculation-settings-component.html',
    styleUrls: ['./calculation-settings-component.css']
})
export class CalculationSettingsComponent {

    public calculationType: string;
    public readonly calculationTypes: string[];
    public shape: string;
    public readonly shapeTypes: string[];

    constructor(private router: Router) {
        this.shapeTypes = [ShapeTypes.RECTANGLE, ShapeTypes.SQUARE, ShapeTypes.CIRCLE];
        this.shape = this.shapeTypes[0];
        this.calculationTypes = [CalculationTypes.FIELD, CalculationTypes.PERIMETER];
        this.calculationType = this.calculationTypes[0];
    }

    public gotoCalculationsView(): void {
        this.router.navigate(['/calculate', this.shape, this.calculationType]);
    }
}