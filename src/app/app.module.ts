import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent}  from './app.component';
import {NavigationComponent} from './components/navigation/navigation.component';
import {CalculationSettingsComponent} from './components/calculationSettings/caclulation-settings-component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FooterComponent} from './components/footer/footer.component';
import {CalculationComponent} from './components/calculation/calculation-component';
import {appRoutes} from './model/routing/AppRoutes';
import {PageNotFoundComponent} from './components/pageNotFound/page-not-found-component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(
            appRoutes
        )],
    declarations: [
        AppComponent,
        CalculationSettingsComponent,
        CalculationComponent,
        PageNotFoundComponent
    ],
    bootstrap: [AppComponent],
    providers: []
})
export class AppModule {
}
