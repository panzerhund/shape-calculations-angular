import {ShapeConstructionPropertiesProvider} from './ShapeConstructionPropertiesProvider';
import {SquareProperties} from '../../../../model/geometry/shape/construction/SquareProperties';
import {CircleProperties} from '../../../../model/geometry/shape/construction/CircleProperties';
import {PropertiesWrapper} from '../../../../model/object/PropertiesWrapper';
describe('ShapeConstructionPropertiesProvider', () => {
    it('should provide wrapped square construction properties', () => {
        const testObj: ShapeConstructionPropertiesProvider = new ShapeConstructionPropertiesProvider();
        const result: PropertiesWrapper = testObj.provide('square');
        expect(result).toEqual(new PropertiesWrapper(<SquareProperties> {side: 0}));
    });
    it('should provide wrapped circle construction properties', () => {
        const testObj: ShapeConstructionPropertiesProvider = new ShapeConstructionPropertiesProvider();
        const result: PropertiesWrapper = testObj.provide('circle');
        expect(result).toEqual(new PropertiesWrapper(<CircleProperties> {radius: 0}));
    });
});