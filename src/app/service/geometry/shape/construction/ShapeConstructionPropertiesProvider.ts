import {CircleProperties} from '../../../../model/geometry/shape/construction/CircleProperties';
import {
    RectangleProperties
} from
    '../../../../model/geometry/shape/construction/RectangleProperties';
import {SquareProperties} from '../../../../model/geometry/shape/construction/SquareProperties';
import {PropertiesWrapper} from '../../../../model/object/PropertiesWrapper';
import {Injectable} from '@angular/core';
import {ShapeTypes} from '../../../../model/geometry/shape/ShapeTypes';
@Injectable()
export class ShapeConstructionPropertiesProvider {
    private shapeProperties: Map<string, PropertiesWrapper>;

    constructor() {
        this.shapeProperties = new Map();
        this.shapeProperties.set(
            ShapeTypes.CIRCLE, new PropertiesWrapper(<CircleProperties> {radius: 0})
        );
        this.shapeProperties.set(
            ShapeTypes.SQUARE, new PropertiesWrapper(<SquareProperties> {side: 0})
        );
        this.shapeProperties.set(
            ShapeTypes.RECTANGLE,
            new PropertiesWrapper(<RectangleProperties> {side0: 0, side1: 0})
        );
    }

    public provide(shapeType: string): PropertiesWrapper {
        return this.shapeProperties.get(shapeType);
    }
}