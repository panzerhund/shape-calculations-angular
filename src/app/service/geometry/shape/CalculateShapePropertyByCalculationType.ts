import {Shape} from '../../../model/geometry/shape/Shape';
import {CalculationTypes} from '../../../model/geometry/shape/CalculationTypes';
import {Injectable} from '@angular/core';

@Injectable()
export class CalculateShapePropertyByCalculationType {

    public calculate(shape: Shape, calculationType: string): number {
        return this.getMapedMethods(shape).get(calculationType).call(shape);
    }

    private getMapedMethods(shape: Shape): Map<string, Function> {
        const map: Map<string, Function> = new Map();
        map.set(CalculationTypes.PERIMETER, shape.getPerimeter);
        map.set(CalculationTypes.FIELD, shape.getField);
        return map;
    }
}