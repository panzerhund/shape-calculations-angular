import {CalculateShapePropertyByCalculationType} from './CalculateShapePropertyByCalculationType';
import {Square} from '../../../model/geometry/shape/Square';
import {SquareProperties} from '../../../model/geometry/shape/construction/SquareProperties';
import {Rectangle} from '../../../model/geometry/shape/Rectangle';
import {RectangleProperties} from '../../../model/geometry/shape/construction/RectangleProperties';
describe('CalculateShapePropertyByCalculationType', () => {
    it('should calculate shape perimeter', () => {
        const testObj: CalculateShapePropertyByCalculationType = new CalculateShapePropertyByCalculationType();
        const result: number = testObj.calculate(new Square(<SquareProperties> {side: 3}), 'perimeter');
        expect(result).toEqual(12);
    });
    it('should calculate shape area', () => {
        const testObj: CalculateShapePropertyByCalculationType = new CalculateShapePropertyByCalculationType();
        const result: number = testObj.calculate(new Rectangle(<RectangleProperties> {side0: 3, side1: 2}), 'field');
        expect(result).toEqual(6);
    });
});