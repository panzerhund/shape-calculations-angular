import {Routes} from '@angular/router';
import {CalculationSettingsComponent} from '../../components/calculationSettings/caclulation-settings-component';
import {CalculationComponent} from '../../components/calculation/calculation-component';
import {PageNotFoundComponent} from '../../components/pageNotFound/page-not-found-component';
export const appRoutes: Routes = [
    {
        path: '',
        component: CalculationSettingsComponent
    },
    {
        path: 'calculate/:shape/:calculationType',
        component: CalculationComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];