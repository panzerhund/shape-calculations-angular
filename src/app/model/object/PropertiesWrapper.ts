export class PropertiesWrapper {
    private props: object = {};

    constructor(obj: object) {
        this.props = Object.assign(this.props, obj);
    }

    public getProperties(): string[] {
        return Object.keys(this.props);
    };

    public setValues(values: object): void {
        Object.assign(this.props, values);
    };
}