import {PropertiesWrapper} from './PropertiesWrapper';
describe('PropertiesWrapper', () => {
    it('should return object keys', () => {
        const testObj: PropertiesWrapper = new PropertiesWrapper({a: 1, b: 2});
        const result: string[] = testObj.getProperties();
        expect(result).toEqual(['a', 'b']);
    });
});