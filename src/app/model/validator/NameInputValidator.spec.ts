import {FormControl} from '@angular/forms';
import {isPositive} from './NumberInputValidator';
describe('NameInputValidator', () => {
    describe('isPositive', () => {
        it('should validate when greater than 0', () => {
            const result: any = isPositive(new FormControl('1'));
            expect(result).toEqual(null);
        });
        it('should not validate when 0', () => {
            const result: any = isPositive(new FormControl('0'));
            expect(result).toEqual({isPositive: {valid: false}});
        });
        it('should not validate when negative', () => {
            const result: any = isPositive(new FormControl('-10.0'));
            expect(result).toEqual({isPositive: {valid: false}});
        });
        it('should not validate when NaN', () => {
            const result: any = isPositive(new FormControl('1a'));
            expect(result).toEqual({isPositive: {valid: false}});
        });
        it('should not validate when NaN', () => {
            const result: any = isPositive(new FormControl('true'));
            expect(result).toEqual({isPositive: {valid: false}});
        });
    });

});