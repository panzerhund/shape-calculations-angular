import {AbstractControl} from '@angular/forms';
export function isPositive(c: AbstractControl): { [key: string]: any } {
    return Number(c.value) > 0 ? null : {
        isPositive: {
            valid: false
        }
    };
}
