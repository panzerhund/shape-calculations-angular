import {Square} from './Square';
import {SquareProperties} from './construction/SquareProperties';
describe('Square', () => {
    it('should calculate area', () => {
        const testObj: Square = new Square(<SquareProperties> {side: 3});
        const result: number = testObj.getField();
        expect(result).toBe(9);
    });
    it('should calculate perimeter', () => {
        const testObj: Square = new Square(<SquareProperties> {side: 3});
        const result: number = testObj.getPerimeter();
        expect(result).toBe(12);
    });
});