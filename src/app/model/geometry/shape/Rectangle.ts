import {RectangleProperties} from './construction/RectangleProperties';
import {Shape} from './Shape';
export class Rectangle implements Shape {
    private readonly side0: number;
    private readonly side1: number;

    constructor(rectangleProperties: RectangleProperties) {
        this.side0 = rectangleProperties.side0;
        this.side1 = rectangleProperties.side1;
    }

    public getPerimeter(): number {
        return 2 * (this.side0 + this.side1);
    }

    public getField(): number {
        return this.side0 * this.side1;
    }
}