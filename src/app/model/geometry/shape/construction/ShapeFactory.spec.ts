import {ShapeFactory} from './ShapeFactory';
import {Shape} from '../Shape';
import {Square} from '../Square';
import {SquareProperties} from './SquareProperties';
import {CircleProperties} from './CircleProperties';
import {Circle} from '../Circle';
import {Rectangle} from '../Rectangle';
import {RectangleProperties} from './RectangleProperties';
describe('ShapeFactory', () => {
    it('should create square', () => {
        const testObj: ShapeFactory = new ShapeFactory();
        const result: Shape = testObj.create('square', {side: 10});
        expect(result).toEqual(new Square(<SquareProperties> {side: 10}));
    });
});
describe('ShapeFactory', () => {
    it('should create circle', () => {
        const testObj: ShapeFactory = new ShapeFactory();
        const result: Shape = testObj.create('circle', {radius: 10});
        expect(result).toEqual(new Circle(<CircleProperties> {radius: 10}));
    });
});
describe('ShapeFactory', () => {
    it('should create rectangle', () => {
        const testObj: ShapeFactory = new ShapeFactory();
        const result: Shape = testObj.create('rectangle', {side0: 10, side1: 1});
        expect(result).toEqual(new Rectangle(<RectangleProperties> {side0: 10, side1: 1}));
    });
});