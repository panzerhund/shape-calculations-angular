import {Shape} from '../Shape';
import {Circle} from '../Circle';
import {CircleProperties} from './CircleProperties';
import {Square} from '../Square';
import {SquareProperties} from './SquareProperties';
import {Rectangle} from '../Rectangle';
import {RectangleProperties} from './RectangleProperties';
import {Injectable} from '@angular/core';
import {ShapeTypes} from '../ShapeTypes';
@Injectable()
export class ShapeFactory {

    public create(type: string, properties: object): Shape {
        let shape: Shape;
        switch (type) {
            case ShapeTypes.CIRCLE:
                shape = new Circle(properties as CircleProperties);
                break;
            case ShapeTypes.SQUARE:
                shape = new Square(properties as SquareProperties);
                break;
            case ShapeTypes.RECTANGLE:
                shape = new Rectangle(properties as RectangleProperties);
                break;
        }
        return shape;
    }
}