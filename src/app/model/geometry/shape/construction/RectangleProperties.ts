export interface RectangleProperties {
    side0: number;
    side1: number;
}