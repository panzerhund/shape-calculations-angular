export class CalculationTypes {
    public static readonly PERIMETER: string = 'perimeter';
    public static readonly FIELD: string = 'field';
}