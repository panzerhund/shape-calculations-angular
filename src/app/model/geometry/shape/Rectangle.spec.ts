import {Rectangle} from './Rectangle';
import {RectangleProperties} from './construction/RectangleProperties';
describe('Rectangle', () => {
    it('should calculate area', () => {
        const testObj: Rectangle = new Rectangle(<RectangleProperties> {side0: 2, side1: 3});
        const result: number = testObj.getField();
        expect(result).toBe(6);
    });
    it('should calculate perimeter', () => {
        const testObj: Rectangle = new Rectangle(<RectangleProperties> {side0: 3, side1: 2});
        const result: number = testObj.getPerimeter();
        expect(result).toBe(10);
    });
});