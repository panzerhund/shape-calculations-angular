import {Shape} from './Shape';
import {CircleProperties} from './construction/CircleProperties';
export class Circle implements Shape {
    private readonly radius: number;
    constructor(circleProperties: CircleProperties) {
        this.radius = circleProperties.radius;
    }

    public getPerimeter(): number {
        return 2 * Math.PI * this.radius;
    }

    public getField(): number {
        return Math.PI * Math.pow(this.radius, 2);
    }
}