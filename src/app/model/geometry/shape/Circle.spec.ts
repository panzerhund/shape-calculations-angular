import {Circle} from './Circle';
import {CircleProperties} from './construction/CircleProperties';
describe('Circle', () => {
    it('should calculate area', () => {
        const testObj: Circle = new Circle(<CircleProperties> {radius: 2});
        const result: number = testObj.getField();
        expect(result).toBeCloseTo(12.5664, 4);
    });
    it('sshould calculate perimeter', () => {
        const testObj: Circle = new Circle(<CircleProperties> {radius: 3});
        const result: number = testObj.getPerimeter();
        expect(result).toBeCloseTo(18.8496, 4);
    });
});