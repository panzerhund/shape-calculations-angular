export interface Shape {
    getPerimeter(): number;
    getField(): number;
}