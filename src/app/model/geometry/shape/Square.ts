import {Shape} from './Shape';
import {SquareProperties} from './construction/SquareProperties';
export class Square implements Shape {
    private readonly side: number;

    constructor(squareProperties: SquareProperties) {
        this.side = squareProperties.side;
    }

    public getPerimeter(): number {
        return 4 * this.side;
    }

    public getField(): number {
        return Math.pow(this.side, 2);
    }
}
