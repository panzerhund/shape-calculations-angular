export class ShapeTypes {
    public static readonly SQUARE: string = 'square';
    public static readonly CIRCLE: string = 'circle';
    public static readonly RECTANGLE: string = 'rectangle';
}